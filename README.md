Learning Locker Docker
======================

Containerized & Orchestrated Learning Locker instances

After Initial Startup
---------------------

After startup, an admin user must be created using the Learning Locker CLI tools.  The command must be run in the container where the cli server is running - the web container if running a standanlone, all-in-one instance.  The easiest way to do so is with `docker-compose exec`:

    docker-compose exec web bash -c "source /src/functions_nvm && cd /learninglocker && node cli/dist/server createSiteAdmin [email] [organisation] [password]"

Note that the cli command must be run from the /learninglocker directory, and if it's run without a login shell, you'll need to source the /src/functions_nvm file for the node command to work.

Alternatively, `docker-compose exec` could be used to get a bash login shell, and the commands can be run from inside the container interactively.

Running without docker-compose
------------------------------

If you choose to run this stack without Docker Compose, there are some links that will need to be maintained in order to work.  Particularly, the nginx.conf file and the env_* files use the following names to connect to their respective services:

* web - the Learning Locker all-in-one server name
* proxy - the Nginx webserver in front of Learning Locker
* mongodb - the Mongodb instance
* redis - the Redis instance

You should either mondify your configs to replace these names with IPs or Hostnames as appropriate for your environment, or preserve these "dns" names as they would be created by Docker Compose.

OAuth Config
------------

Should be configuratble with these two files:

ui/src/utils/oauth.js

lib/constants/routes.js

Looks like there's case statements for a bunch of not-implemented solutions + Google OAuth.  We could probably set this up for our OAuth stuff, but would require maintaining a patch.


Running outside of Duke Univeristy
----------------------------------

Two containers not included in this repository are used for the database backends for this service: Redis and MongoDB.  These are standard, containerized versions of both with little difference from the upstream packages used, packaged by Duke University OIT.  If you use this stack elsewhere, you'll need to build your own containers for Redis and MongoDB, and adjust the docker-compose.yml accordingly.

The proxy and web containers expect a Centos7 based base-image with the [Runit init system](http://smarden.org/runit/) and EPEL repo installed.  Additionally, the web container has Nodejs installed via NVM, and the Nginx container contains, unsurprisingly, and Nginx webserver.  These Dockerfiles should work well enough if you replicate those setups.

Something to watch out for - make sure your configuration files are set to listen to 0.0.0.0 instead of 127.0.0.1 for connections.

