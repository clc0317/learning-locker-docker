pipeline {
  agent {
    node {
      label 'atomic'
    }
  }
  triggers {
    upstream(
      upstreamProjects: 'el7-nginx/master, el7-nvm/master',
      threshold: hudson.model.Result.SUCCESS
    )
  }
  stages {
    stage ('Build Images') {
      steps {
        parallel (
          Standalone: {
            sh '''
               source /build/pipeline_vars
               docker build -t learninglocker-standalone -f Dockerfile-standalone .
            '''
          },
          Nginx: {
            sh '''
               source /build/pipeline_vars
               docker build -t learninglocker-proxy -f Dockerfile-nginx .
            '''
          }
        )
      }
    }
    stage('Test') {
      steps {
        parallel (
          Integrity_standalone: {
            sh '''
               source /build/pipeline_vars
               docker run --rm \
                          --entrypoint=${TEST_ENTRYPOINT} \
                          -t learninglocker-standalone ${TEST_CMD}
               '''
          },
          Integrity_proxy: {
            sh '''
               source /build/pipeline_vars
               docker run --rm \
                          --entrypoint=${TEST_ENTRYPOINT} \
                          -t learninglocker-proxy ${TEST_CMD}
               '''
          },
          Unit: {
            echo "Enter Unit Tests Here"
          },
          Integration: {
            echo "Enter Integration Tests Here"
          },
          SysSecurity_standalone: {
            sh '''
              source /build/pipeline_vars
              TMP_FILE=/tmp/learninglocker-standalone 
              fconsec-enumerate-local-docker > learninglocker-standalone 
              fconsec-scan-packagelist-cve -f 7.0 learninglocker-standalone
            '''
          },
          SysSecurity_proxy: {
            sh '''
              source /build/pipeline_vars
              TMP_FILE=/tmp/learninglocker-proxy 
              fconsec-enumerate-local-docker > learninglocker-proxy 
              fconsec-scan-packagelist-cve -f 7.0 learninglocker-proxy
            '''
          }
        )
      }
    }
    stage('Tag Images') {
      steps {
        parallel (
          Tag_standalone: {
            sh '''
               source /build/pipeline_vars
               docker tag learninglocker-standalone stevedore-repo.oit.duke.edu/learninglocker-standalone:${BUILD_NUMBER}
               docker tag learninglocker-standalone stevedore-repo.oit.duke.edu/learninglocker-standalone:latest
               '''
          },
          Tag_proxy: {
            sh '''
               source /build/pipeline_vars
               docker tag learninglocker-proxy stevedore-repo.oit.duke.edu/learninglocker-proxy:${BUILD_NUMBER}
               docker tag learninglocker-proxy stevedore-repo.oit.duke.edu/learninglocker-proxy:latest
               '''
          }
        )
      }
    }
    stage('Push Images') {
      steps {
        parallel (
          Push_standalone: {
            sh '''
               source /build/pipeline_vars
               docker push stevedore-repo.oit.duke.edu/learninglocker-standalone:${BUILD_NUMBER}
               docker push stevedore-repo.oit.duke.edu/learninglocker-standalone:latest
               '''
          },
          Push_proxy: {
            sh '''
               source /build/pipeline_vars
               docker push stevedore-repo.oit.duke.edu/learninglocker-proxy:${BUILD_NUMBER}
               docker push stevedore-repo.oit.duke.edu/learninglocker-proxy:latest
               '''
          }
        )
      }
    }
  }
  post { 
    always { 
      sh '''
         source /build/pipeline_vars
         docker rmi stevedore-repo.oit.duke.edu/learninglocker-standalone:${BUILD_NUMBER}
         docker rmi stevedore-repo.oit.duke.edu/learninglocker-standalone:latest
         docker rmi stevedore-repo.oit.duke.edu/learninglocker-proxy:${BUILD_NUMBER}
         docker rmi stevedore-repo.oit.duke.edu/learninglocker-proxy:latest
      '''
    }
    failure {
      mail(from: "doozer@build.docker.oit.duke.edu",
        to: "christopher.collins@duke.edu",
        subject: "LearningLocker Image Builds Failed",
        body: "Something failed with the LearningLocker image builds: https://build.docker.oit.duke.edu/blue/organizations/jenkins/learninglocker/activity")
    }
  }
}
